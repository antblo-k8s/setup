# Become root
```
sudo su
```

# Common server setup
```
swapoff -a
sed -e '/\/swap.*/ s/^#*/#/' -i /etc/fstab
apt install -y open-iscsi
```

# Setup local servers
```
ADDRESS=192.168.1.100
```

## Remove lid shutdown on laptops
```
sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/g' /etc/systemd/logind.conf
sudo systemctl restart systemd-logind
```

# Setup remote servers
```
ADDRESS=antblo.com
```

# Setup first control node
```
curl -sfL https://get.k3s.io | sh -s - server --cluster-init --disable servicelb --disable traefik --disable local-storage
cat /var/lib/rancher/k3s/server/node-token
echo 
cat /etc/rancher/k3s/k3s.yaml
```

# Setup HA control nodes
```
K3S_TOKEN=
curl -sfL https://get.k3s.io | K3S_TOKEN=$K3S_TOKEN ADDRESS=$ADDRESS sh -s - server --server https://$ADDRESS:6443
```

# Setup worker nodes
```
K3S_TOKEN=
curl -sfL https://get.k3s.io | K3S_TOKEN=$K3S_TOKEN K3S_URL=https://$ADDRESS:6443 sh -s -
```

# Setup repo

## Create deploy token:
(May need account access token with read/write to repo/registry)
https://gitlab.com/groups/GROUP_HERE/-/settings/repository/deploy_token/create#js-deploy-tokens

## (Optionally) Update Argocd chart in this repo:
```
helm repo update
```

## Create values-override-file:
```
code ~/.kube/cluster_creds.yaml
```

## Init argocd for dev cluster:
```
helm upgrade -i argocd -n argocd --create-namespace --values ~/.kube/cluster_creds.yaml --set env=master .
```

## Init argocd for prod cluster:
```
helm upgrade -i argocd -n argocd --create-namespace --values ~/.kube/cluster_creds.yaml --set env=prod --kubeconfig ~/.kube/prod-config .
```

## Update argocd:
```
helm upgrade argocd -n argocd --values ~/.kube/cluster_creds.yaml .
helm upgrade argocd -n argocd --values ~/.kube/cluster_creds.yaml --kubeconfig ~/.kube/prod-config .
```
