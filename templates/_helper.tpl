{{- define "imagePullSecret" }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .Values.registry .Values.git_username .Values.git_password .Values.git_email (printf "%s:%s" .Values.git_username .Values.git_password | b64enc) | b64enc }}
{{- end }}

{{- define "antblo_domain" -}}
{{- if eq .Values.env "prod" -}}
antblo.com
{{- else -}}
antblo.loc
{{- end -}}
{{ end -}}
